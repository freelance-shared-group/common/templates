# Gitlab template to apply CI/CD pipelines
## Introduction
To make building and deploy new application automatically please include this repository to your CI/CD pipeline to make everything working as expected

## How to setup pipeline
### Setup CI flow
Create file .gitlab-ci.yml
```
include:
  - project: 'freelance-shared-group/common/templates'
    file: '/main.gitlab-ci.yml'
```
The pipeline will support database mysql, redis and ganache service for testing and benchmarking

### Setup CD flow
#### Vercel
To deploy application to vercel we would need to define these variables `VERCEL_TOKEN`, `VERCEL_ORG_ID` and `VERCEL_PROJECT_ID` to unlock job for deploying to vercel platform.
The deployment strategy will be on two flow, `preview` and `production` and you can easily rollback when any issue happen by clicking rollback jobs revert changing immediatelly.

### Setup load test system
The load test job is defined using k6 only and it requires developer to define where to load load script by define the variable `K6_TEST_FILE`. The target will be the preview system where we can perform load test to validate the performance of new code.
